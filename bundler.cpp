/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "budho/bundler.h"
#include <fstream>
#include <fort.hpp>
#include <boost/process.hpp>
#include <termcolor/termcolor.hpp>
#include <boost/format.hpp>
#include <boost/filesystem/operations.hpp>
#include <cmrc/cmrc.hpp>

CMRC_DECLARE(resources);

budho::bundler::bundler(int argc, char** argv): _argc(argc), _argv(argv){}

void budho::bundler::parse(){
    _description.add_options()
        ("list,l", "Show list of all components bundled inside")
        ("component,c", boost::program_options::value<std::string>()->default_value(""),   "Component to generate")
        ("generate,g", "Generate Components in the directory")
        ("in,i", boost::program_options::value<std::string>(),   "input directory")
        ("out,o", boost::program_options::value<std::string>(),   "output directory name")
        ("help,h", "This help message");

    try{
        boost::program_options::store(boost::program_options::parse_command_line(_argc, _argv, _description), _vm);
        boost::program_options::notify(_vm);
    }catch(const boost::program_options::error &ex){
        std::cerr << termcolor::red << ex.what() << termcolor::reset << std::endl;
        std::cout << _description << std::endl;
        
        throw;
    }
}

void budho::bundler::generate(const boost::filesystem::path& base){
    std::size_t count = 0;
    for(const auto& kv: _components){
        boost::filesystem::path component_path = base / (kv.first+".vue");
        std::ofstream stream(component_path.string());
        stream << kv.second;
        stream.close();
        std::cout << termcolor::colorize << termcolor::blue << boost::format("bundler >> generating component %1% -> %2%/%1%.vue") % kv.first % base.string() << termcolor::reset << std::endl;
        ++count;
    }
    std::cout << "bundler >> Generated " << count << " components" << std::endl;
}

void budho::bundler::link_all(const boost::filesystem::path& source, const boost::filesystem::path& target){
    if (boost::filesystem::is_directory(source)) {
        if(!boost::filesystem::exists(target)){
            boost::filesystem::create_directory(target);
            std::cout << termcolor::colorize << termcolor::magenta << boost::format("bundler >> creating directory %1%") % target << termcolor::reset << std::endl;
        }
        for (boost::filesystem::directory_entry& item : boost::filesystem::directory_iterator(source)) {
            auto tgt = target/item.path().filename();
            boost::filesystem::file_type type = boost::filesystem::symlink_status(tgt).type();
            bool exists = (boost::filesystem::exists(tgt) || (type != boost::filesystem::file_not_found && type != boost::filesystem::status_error));
            if(!exists){
                std::cout << termcolor::colorize << termcolor::magenta << boost::format("bundler >> creating symlink %1% -> %2%") % item.path() % tgt << termcolor::reset << std::endl;
                boost::filesystem::create_symlink(item.path(), tgt);
            }else{
                std::cout << termcolor::colorize << termcolor::magenta << boost::format("bundler >> symlink %1% exists") % tgt << termcolor::reset << std::endl;
            }
        }
    }
}

void budho::bundler::copy(const boost::filesystem::path& source, const boost::filesystem::path& target){
    if (boost::filesystem::exists(target)){
        throw std::runtime_error(target.generic_string() + " exists");
    }

    if (boost::filesystem::is_directory(source)) {
        boost::filesystem::create_directories(target);
        for (boost::filesystem::directory_entry& item : boost::filesystem::directory_iterator(source)) {
            copy(item.path(), target/item.path().filename());
        }
    } 
    else if (boost::filesystem::is_regular_file(source)) {
        boost::filesystem::copy(source, target);
    } 
    else {
        throw std::runtime_error(target.generic_string() + " not dir or file");
    }
}

void budho::bundler::pack(){
    if(_vm.count("help")){
        std::cout << _description;
        return;
    }

    if(_vm.count("list")){
        boost::filesystem::path base;
        if(_vm.count("out")){
            base = _vm["out"].as<std::string>();
        }
        fort::char_table table;
        table.set_border_style(FT_PLAIN_STYLE);
        table << fort::header << "Name" << "Path" << "Bytes" << fort::endr;
        for(const auto& kv: _components){
            if(_vm.count("out")){
                table << kv.first << boost::filesystem::absolute(base / (kv.first+".vue")) << kv.second.size() << fort::endr;
            }else{
                table << kv.first << "_OUTPUT_PATH_/"+(kv.first+".vue") << kv.second.size() << fort::endr;
            }
        }
        std::cout << table.to_string() << std::endl;
        if(!_vm.count("out")){
            std::cout << termcolor::colorize << termcolor::magenta << "no output path specified use --out OUTPUT_PATH" << termcolor::reset << std::endl;
        }
        return;
    }

    if(_vm.count("generate")){
        if(!_vm.count("in") || !_vm.count("out")){
            std::cout << termcolor::colorize << termcolor::red << "-g requires --in and --out" << termcolor::reset << std::endl;
            return;
        }
        
        std::string in  = _vm["in"].as<std::string>();
        std::string out = _vm["out"].as<std::string>();
        boost::filesystem::path sources = in;
        boost::filesystem::path base = out;
        
        npm_init(sources);
        webpack_init(sources);
        budho_js(sources);
        
        std::cout << termcolor::colorize << termcolor::magenta << boost::format("bundler >> generating using in:%1% out:%2%") % sources % base << termcolor::reset << std::endl;
        if (boost::filesystem::exists(out)) {
            std::cout << termcolor::colorize << termcolor::cyan << "bundler >> directory "+out+" already exist" << termcolor::reset << std::endl;
        }
        link_all(sources, base);
        boost::filesystem::path node_modules = sources / "node_modules";
        if (!boost::filesystem::exists(node_modules)) {
            boost::filesystem::create_directory(node_modules);
        }
        
        auto tgt = base / "node_modules";
        boost::filesystem::file_type type = boost::filesystem::symlink_status(tgt).type();
        bool exists = (boost::filesystem::exists(tgt) || (type != boost::filesystem::file_not_found && type != boost::filesystem::status_error));
        
        if(!exists){
            std::cout << termcolor::colorize << termcolor::magenta << boost::format("bundler >> creating symlink %1% -> %2%") % node_modules % tgt << termcolor::reset << std::endl;
            boost::filesystem::create_symlink(node_modules, tgt);
        }else{
            std::cout << termcolor::colorize << termcolor::magenta << boost::format("bundler >> symlink %1% exists") % tgt << termcolor::reset << std::endl;
        }
        
        generate(base);
        npm_install(base);
        webpack(base);
    }
}

void budho::bundler::npm_init(const boost::filesystem::path& base){
    boost::filesystem::path package = base / "package.json";
    if(!boost::filesystem::exists(package)){
        boost::filesystem::path npm = boost::process::search_path("npm");
        if(npm.string().empty()){
            std::cout << termcolor::red << "npm not found in path. please install npm" << termcolor::reset << std::endl;
            return;
        }
        std::cout << ">> npm init --yes" << std::endl;
        boost::process::child init(npm, "init", "--yes", boost::process::std_out > stdout, boost::process::std_err > stderr, boost::process::std_in < stdin, boost::process::start_dir = base);
        init.wait();
        std::string essentials[] = {"vue", "vue-loader", "vue-style-loader", "vue-template-compiler", "css-loader", "axios", "webpack", "webpack-cli", "babel-loader", "@babel/core", "@babel/preset-env"};
        for(const std::string& pkg: essentials){
            std::cout << ">> npm install " << pkg << " --save" << std::endl;
            boost::process::child install(npm, "install", pkg, "--save", boost::process::std_out > stdout, boost::process::std_err > stderr, boost::process::std_in < stdin, boost::process::start_dir = base);
            install.wait();
        }
        
        boost::filesystem::path index_js = base / "index.js";
        if(!boost::filesystem::exists(index_js)){
            std::ofstream stream(index_js.c_str());
            auto fs = cmrc::resources::get_filesystem();
            cmrc::file index_js_contents = fs.open("budho/resources/index.js");
            std::copy(index_js_contents.cbegin(), index_js_contents.cend(), std::ostream_iterator<char>(stream));
            stream.close();
            std::cout << termcolor::cyan << "created index.js" << termcolor::reset << std::endl;
        }
    }else{
        std::cout << termcolor::cyan << boost::format("file %1% exists") % boost::filesystem::canonical(package) << termcolor::reset << std::endl;
    }
}

void budho::bundler::npm_install(const boost::filesystem::path& base){
    boost::filesystem::path packed_cpp   = base / "packed.cpp";
    boost::filesystem::path package_json = base / "package.json";
    
    if(boost::filesystem::exists(packed_cpp)){
        std::time_t packed_cpp_time   = boost::filesystem::last_write_time(packed_cpp);
        std::time_t package_json_time = boost::filesystem::last_write_time(package_json);
        
        if(package_json_time < packed_cpp_time){
            std::cout << termcolor::cyan << boost::format("skipping `npm install` because there is no modification to %1%") % boost::filesystem::canonical(package_json) << termcolor::reset << std::endl;
            return;
        }
    }
    boost::filesystem::path npm = boost::process::search_path("npm");
    if(npm.string().empty()){
        std::cout << termcolor::red << "bundler >> npm not found in path. please install npm" << termcolor::reset << std::endl;
        return;
    }
    boost::process::child install(npm, "install", boost::process::std_out > stdout, boost::process::std_err > stderr, boost::process::std_in < stdin, boost::process::start_dir = base);
    install.wait();
}

void budho::bundler::webpack_init(const boost::filesystem::path& base){
    boost::filesystem::path webpack_config = base / "webpack.config.js";
    if(!boost::filesystem::exists(webpack_config)){
        std::ofstream stream(webpack_config.c_str());
        auto fs = cmrc::resources::get_filesystem();
        cmrc::file webpack_config_file = fs.open("budho/resources/webpack.config.js");
        std::copy(webpack_config_file.cbegin(), webpack_config_file.cend(), std::ostream_iterator<char>(stream));
        stream.close();
        std::cout << termcolor::magenta << boost::format("creating empty webpack.config.js on %1%") % boost::filesystem::canonical(webpack_config) << termcolor::reset << std::endl;
    }else{
        std::cout << termcolor::cyan << boost::format("file %1% exists") % boost::filesystem::canonical(webpack_config) << termcolor::reset << std::endl;
    }
}

void budho::bundler::budho_js(const boost::filesystem::path& base){
    boost::filesystem::path budho_js = base / "budho.js";
    if(!boost::filesystem::exists(budho_js)){
        std::ofstream stream(budho_js.c_str());
        auto fs = cmrc::resources::get_filesystem();
        cmrc::file budho_js_content = fs.open("budho/resources/budho.js");
        std::copy(budho_js_content.cbegin(), budho_js_content.cend(), std::ostream_iterator<char>(stream));
        stream.close();
        std::cout << termcolor::magenta << boost::format("creating empty webpack.config.js on %1%") % boost::filesystem::canonical(budho_js) << termcolor::reset << std::endl;
    }else{
        std::cout << termcolor::cyan << boost::format("file %1% exists") % boost::filesystem::canonical(budho_js) << termcolor::reset << std::endl;
    }
}

void budho::bundler::webpack(const boost::filesystem::path& base){
    boost::filesystem::path npx = boost::process::search_path("npx");
    if(npx.string().empty()){
        std::cout << termcolor::red << "bundler >> npx not found in path. please install npx" << termcolor::reset << std::endl;
        return;
    }
    boost::process::child webpack(npx, "webpack", boost::process::std_out > stdout, boost::process::std_err > stderr, boost::process::std_in < stdin, boost::process::start_dir = base);
    webpack.wait();
}
