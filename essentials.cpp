/*
 * Copyright (c) 2019, <copyright holder> <email>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY <copyright holder> <email> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <copyright holder> <email> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "budho/essentials.h"

budho::require::require(const std::initializer_list<std::string>& things): _braced(true), _mode(Mode::unspecified){
    std::copy(things.begin(), things.end(), std::back_inserter(_things));
}
budho::require::require(const std::string& thing): _braced(false), _mode(Mode::unspecified){
    _things.push_back(thing);
}
budho::require::require(Mode mode): _braced(false), _mode(mode){
    if(_mode == unspecified){
        _mode = self;
    }
}
budho::require& budho::require::from(const std::string& path){
    _path = path;
    return *this;
}

void budho::reflect(const cmrc::embedded_filesystem& fs){
    std::function<void (const std::string&)> iterate = [&fs, &iterate](const std::string dirname){
        cmrc::directory_iterator dir = fs.iterate_directory(dirname);
        for(cmrc::directory_iterator it = dir.begin(); it != dir.end(); ++it){
            cmrc::directory_entry entry = *it;
            std::string name = entry.filename();
            if(entry.is_directory()){
                // create directory
                iterate(name);
            }else if(entry.is_file()){
                // create the file
            }
        }
    };
    iterate("");
}
