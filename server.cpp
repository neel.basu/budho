/*
 * Copyright (c) 2019, <copyright holder> <email>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY <copyright holder> <email> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <copyright holder> <email> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "budho/server.h"
#include <boost/process.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/algorithm/string/join.hpp>
#include <cmrc/cmrc.hpp>
#include <inja/inja.hpp>

CMRC_DECLARE(resources);

budho::server::server(int argc, char** argv): _argc(argc), _argv(argv), _description("Budho (বুধো) web application development framework"), _bundle(argc, argv){
    
}

void budho::server::run(int threads){
    std::vector<std::thread> v;
    v.reserve(threads - 1);
    for(auto i = threads - 1; i > 0; --i)
    v.emplace_back([this]{
        _io.run();
    });
    _io.run();
    for(auto& t : v){
        t.join();
    }
}

void budho::server::welcome() const{
    auto fs = cmrc::resources::get_filesystem();
    cmrc::file welcome = fs.open("budho/resources/welcome.txt");
    std::string content(welcome.cbegin(), welcome.cend());
    
    inja::Environment env;
    std::string rendered = env.render(content, {
        {"host", _vm["host"].as<std::string>()},
        {"port", _vm["port"].as<int>()},
        {"threads", _vm["threads"].as<int>()}
    });
    
    std::cout << rendered << std::endl;
 
    boost::filesystem::path docroot = boost::filesystem::absolute(_vm["documents"].as<std::string>());
    bool docroot_exists = boost::filesystem::exists(docroot) && boost::filesystem::is_directory(docroot);
    std::cout << termcolor::colorize << " Dᴏᴄᴜᴍᴇɴᴛ Rᴏᴏᴛ  " << (docroot_exists ? termcolor::cyan : termcolor::red) << docroot.string() << termcolor::reset << std::endl;
       
    boost::filesystem::path template_root = boost::filesystem::absolute(_vm["templates"].as<std::string>());
    bool template_exists = boost::filesystem::exists(template_root) && boost::filesystem::is_directory(template_root);
    std::cout << termcolor::colorize << " Tᴇᴍᴘʟᴀᴛᴇ Rᴏᴏᴛ  " << (template_exists ? termcolor::cyan : termcolor::red) << template_root.string() << termcolor::reset << std::endl;
    
    std::cout << std::endl;
}

void budho::server::parse(){
    _description.add_options()
        ("host,H", boost::program_options::value<std::string>()->default_value("0.0.0.0"),  "Server IP")
        ("port,P", boost::program_options::value<int>()->default_value(9198),               "Server port")
        ("threads,J", boost::program_options::value<int>()->default_value(2),               "Threads")
        ("documents,D", boost::program_options::value<std::string>()->default_value(""), "WWW Document root directory")
        ("templates,T", boost::program_options::value<std::string>()->default_value(""), "Template root directory (may not be inside documents)")
        ("table,t", "Show table of URL patterns")
        ("components", "List components inside the application")
        ("component", boost::program_options::value<std::string>(), "Definition of a Component")
        ("help,h", "This help message");
        
    try{
        boost::program_options::store(boost::program_options::parse_command_line(_argc, _argv, _description), _vm);
        boost::program_options::notify(_vm);
    }catch(const boost::program_options::error &ex){
        std::cerr << termcolor::red << ex.what() << termcolor::reset << std::endl;
        std::cout << _description << std::endl;
        
        throw;
    }
}
