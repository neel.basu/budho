/*
 * Copyright (c) 2020, <copyright holder> <email>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY <copyright holder> <email> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <copyright holder> <email> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "budho/util.h"

std::string budho::util::stringify(const nlohmann::json& value){
    nlohmann::json::value_t type = value.type();
    switch(type){
        case nlohmann::json::value_t::null:
            return "null";
            break;
        case nlohmann::json::value_t::boolean:
            return boost::lexical_cast<std::string>(value.get<bool>());
            break;
        case nlohmann::json::value_t::number_integer:
            return boost::lexical_cast<std::string>(value.get<int>());
            break;
        case nlohmann::json::value_t::number_unsigned:
            return boost::lexical_cast<std::string>(value.get<unsigned>());
            break;
        case nlohmann::json::value_t::number_float:
            return boost::lexical_cast<std::string>(value.get<double>());
            break;
        case nlohmann::json::value_t::string:
            return value.get<std::string>();
            break;
        case nlohmann::json::value_t::object:
            return value.dump();
            break;
        case nlohmann::json::value_t::array:
            return value.dump();
            break;
        default:
            return "";
    }
}
