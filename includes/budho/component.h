/*
 * Copyright (c) 2018, Sunanda Bose (Neel Basu) (neel.basu.z@gmail.com) 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 * 
 *  * Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer. 
 *  * Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE. 
 */


#ifndef BUDHO_COMPONENT_H
#define BUDHO_COMPONENT_H

#include <string>
#include <ostream>
#include <udho/application.h>
#include <inja/inja.hpp>
#include <nlohmann/json.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <budho/essentials.h>
#include <budho/local.h>
#include <udho/router.h>
#include <udho/application.h>
#include <rumal/declares.hpp>
#include <boost/tti/has_member_function.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>

namespace budho{
namespace detail{
namespace buffers{
    std::string component();
}

namespace passes{

template <typename T>
struct has_data{
    typedef char one;
    struct two { char x[2]; };

    template <typename C> static one test(typeof(&C::data)) ;
    template <typename C> static two test(...);    

    enum { value = sizeof(test<T>(0)) == sizeof(char) };
};

template <typename ComponentT, bool HasData = has_data<ComponentT>::value>
struct data_{
    ComponentT& _component;
    
    data_(ComponentT& component): _component(component){}
    nlohmann::json operator()(const udho::request_type& req, const nlohmann::json& props, const nlohmann::json& d){
        return _component.data(req, props, d);
    }
};

template <typename ComponentT>
struct data_<ComponentT, false>{
    ComponentT& _component;
    
    data_(ComponentT& component): _component(component){}
    nlohmann::json operator()(const udho::request_type& /*req*/, const nlohmann::json& /*props*/, const nlohmann::json& d){
        return d;
    }
};

template <typename ComponentT>
nlohmann::json data(ComponentT& component, const udho::request_type& req, const nlohmann::json& props, const nlohmann::json& d){
    data_<ComponentT> dt(component);
    return dt(req, props, d);
}

}

namespace css{
    template <typename T>
    struct scoped_{
        bool value() const{return false;}
    };
    
    template <std::uint32_t... Flags>
    struct scoped_<rumal::fragment<Flags...>>{
        rumal::fragment<Flags...> frag;
        
        bool value() const{return frag.template has<rumal::css::scoped>();}
    };
    
    template <typename T>
    bool scoped(const T& /*frag*/){
        return scoped_<T>().value();
    }
}

template <typename Function, template <typename> class CompositorT=udho::compositors::transparent>
struct method_overload{    
    typedef Function                                                                 function_type;
    typedef method_overload<Function, CompositorT>                                   self_type;
    typedef typename udho::internal::function_signature<Function>::return_type       return_type;
    typedef typename udho::internal::function_signature<Function>::tuple_type        tuple_type;
    typedef typename udho::internal::function_signature<Function>::arguments_type    arguments_type;
    typedef CompositorT<return_type>                                                 compositor_type;
    typedef typename compositor_type::response_type                                  response_type;
    
    std::string        _name;
    function_type      _function;
    compositor_type    _compositor;
    
    method_overload(function_type f, compositor_type compositor=compositor_type()): _function(f), _compositor(compositor){}
    method_overload(const self_type& other): _name(other._name), _function(other._function), _compositor(other._compositor){}

    self_type& operator=(const std::string& name){
        _name = name;
        return *this;
    }
    /**
     * check number of arguments supplied on runtime and number of arguments with which this overload has been prepared at compile time.
     */
    bool feasible(const std::string& subject) const{
        if(_name.empty()){
            return false;
        }
        return udho::util::urldecode(subject) == _name;
    }
    template <typename T>
    return_type call(const T& req, const std::vector<std::string>& args){
        std::deque<std::string> argsq;
        std::copy(args.begin(), args.end(), std::back_inserter(argsq));
        tuple_type tuple;
        if(::boost::tuples::length<tuple_type>::value > args.size()){
            argsq.push_front("");
        }
        std::vector<std::string> args_str;
        std::copy(argsq.begin(), argsq.end(), std::back_inserter(args_str));
        udho::internal::arguments_to_tuple(tuple, args_str);
        boost::get<0>(tuple) = req;
        arguments_type arguments = udho::internal::to_fusion(tuple);
        // https://www.boost.org/doc/libs/1_68_0/libs/fusion/doc/html/fusion/functional/invocation/functions/invoke.html
        return boost::fusion::invoke(_function, arguments);
    }
    template <typename T>
    response_type operator()(const T& req, const std::vector<std::string>& args){
        return_type ret = call(req, args);
        return _compositor(req, std::move(ret));
    }
    private:
        self_type& operator=(const self_type& other){
            _function = other._function;
            _name = other._name;
            _compositor = other._compositor;
            return *this;
        }
};

template <typename T>
struct deduced_;

template <typename R, typename C, typename... Args>
struct deduced_<R (C::* ) (Args...)>{
    typedef C that_type;
    typedef R (C::* actual_function_type) (Args...);
    typedef boost::function<R (C*, Args...)> base_function_type;
    typedef udho::internal::bind_first<base_function_type> binder_type;
    typedef typename binder_type::object_type object_type;
    typedef typename binder_type::reduced_function_type deduced_function_type;
    
    actual_function_type _function;
    
    constexpr deduced_(actual_function_type function): _function(function){}
    constexpr deduced_function_type bound(C* that) const{
        binder_type binder(that, base_function_type(_function));
        return binder.reduced();
    }
};

template <typename R, typename C, typename... Args>
struct deduced_<R (C::* ) (Args...) const>{
    typedef C that_type;
    typedef R (C::* actual_function_type) (Args...) const;
    typedef boost::function<R (const C*, Args...)> base_function_type;
    typedef udho::internal::bind_first<base_function_type> binder_type;
    typedef const typename binder_type::object_type object_type;
    typedef typename binder_type::reduced_function_type deduced_function_type;
    
    actual_function_type _function;
       
    constexpr deduced_(actual_function_type function): _function(function){}
    constexpr deduced_function_type bound(const C* that) const{
        binder_type binder(that, base_function_type(_function));
        return binder.reduced();
    }
};

template <typename F>
struct method_wrapper: deduced_<F>{
    typedef deduced_<F> base_type;
    typedef typename base_type::that_type that_type;
    typedef typename base_type::actual_function_type actual_function_type;
    typedef typename base_type::deduced_function_type deduced_function_type;
    typedef method_wrapper<F> self_type;
    
    const char* _name;
    that_type*  _that;
    
    constexpr explicit method_wrapper(actual_function_type f, const char* name): base_type(f), _name(name), _that(0x0){}
    self_type& set(that_type* that){
        _that = that;
        return *this;
    }
    template <template <typename> class CompositorT=udho::compositors::transparent>
    auto unwrap(CompositorT<typename udho::internal::function_signature<deduced_function_type>::return_type> compositor = CompositorT<typename udho::internal::function_signature<deduced_function_type>::return_type>()) const{
        auto m = detail::method_overload<deduced_function_type, CompositorT>(base_type::bound(_that), compositor);
        m = _name;
        return m;
    }
    /**
     * raw content delivery using transparent compositor
     */
    auto raw() const{
        return unwrap();
    }
    /**
     * applies a mimed compositor on the return
     * @param mime returned mime type
     */
    auto mimed(std::string mime) const{
        return unwrap(udho::compositors::mimed<typename udho::internal::function_signature<deduced_function_type>::return_type>(mime));
    }
    /**
     * shorthand for html mime type
     */
    auto html() const{
        return mimed("text/html");
    }
    /**
     * shorthand for plain text mime type
     */
    auto plain() const{
        return mimed("text/plain");
    }
    /**
     * shorthand for json mime type
     */
    auto json() const{
        return mimed("application/json");
    }
};

template <typename SelfT>
struct methods_helper{
    SelfT&  _self;
    
    methods_helper(SelfT& ths): _self(ths){}
    template <typename MethodT>
    methods_helper& add(MethodT m){
        _self._methods.insert(std::make_pair(m._name, m));
        return *this;
    }
};

}

template <typename F>
constexpr auto method(F f, const char* name){
    return detail::method_wrapper<F>(f, name);
}
    
/**
 * @todo write docs
 */
template <typename T>
struct component: public udho::application<component<T>>{
    typedef component<T> self_type;
    typedef udho::application<component<T>> base_type;
    typedef self_type component_type;
    typedef boost::beast::http::request<boost::beast::http::string_body>                                    call_request_type;
    typedef boost::beast::http::response<boost::beast::http::string_body>                                   call_response_type;
    typedef boost::function<call_response_type (const call_request_type&, const std::vector<std::string>&)> call_method_type;
    typedef std::map<std::string, call_method_type>                                                         call_storage_type;
    typedef detail::methods_helper<self_type>                                                               method_storage_type;
    
    struct mixin_helper{
        self_type&  _component;
        std::string _name;
        
        mixin_helper(self_type& component, const std::string& name): _component(component), _name(name){}
        void from(const std::string& filename){
            _component.mixin(_name, filename);
        }
    };
    
    std::vector<std::string>      _mixins;
    std::vector<budho::require>   _imports;
    std::set<std::string>         _watchers;
    call_storage_type             _methods;
    method_storage_type           _method_store;
    
    using base_type::name;

    component(const std::string& name): base_type(name), _method_store(*this){}
    template <typename StreamT>
    StreamT& generate(StreamT& stream) const{
        std::string vue_component_template = detail::buffers::component();
        
        nlohmann::json properties = static_cast<const T*>(this)->props();
        
        auto css_ = static_cast<const T*>(this)->css();
        bool is_scoped = detail::css::scoped(css_);
        
        std::string html  = boost::lexical_cast<std::string>(static_cast<const T*>(this)->html());
        std::string css   = boost::lexical_cast<std::string>(css_);
        std::string props = boost::lexical_cast<std::string>(properties);
        
        std::vector<std::string> methods;
        boost::copy(_methods | boost::adaptors::map_keys, std::back_inserter(methods));

        nlohmann::json vue_component_data = {
            {"name", name()},
            {"html",  html},
            {"css",   css},
            {"scoped", is_scoped},
            {"props", props},
            {"mixins", _mixins},
            {"watchers", _watchers},
            {"methods", methods},
            {"url", {
                {"data",   (boost::format("/%1%/data") % name()).str()},
                {"call", (boost::format("/%1%/call") % name()).str()}
            }}
        };
        inja::Environment env;
        env.set_expression("{@", "@}");
        stream << env.render(vue_component_template, vue_component_data);
        return stream;
    }
    template <typename RouterT>
    auto route(RouterT& router){
        auto routed = router | (base_type::post(&self_type::_data).json() = "^/data$")
                             | (base_type::post(&self_type::_call).json() = "^/call");
        return static_cast<T&>(*this).index(routed);
    }
    self_type& watch(const std::string& prop){
        _watchers.insert(prop);
        return *this;
    }
    nlohmann::json _data(const udho::request_type& req){
        std::string body = req.body();
        nlohmann::json json = nlohmann::json::parse(body, nullptr, false);
        
        nlohmann::json props = json["props"];
        nlohmann::json content = json["content"];
        
        return detail::passes::data(static_cast<T&>(*this), req, props, content);
    }
    auto _call(const udho::request_type& req){
        std::string body = req.body();
        nlohmann::json json = nlohmann::json::parse(body, nullptr, false);
        
        std::string name = json["name"].get<std::string>();
        nlohmann::json argsj = json["args"];
        
        std::vector<std::string> args;
        for(const nlohmann::json& arg: argsj){
            args.push_back(budho::util::stringify(arg));
        }
        
        for(auto m: _methods){
            if(m.first == name){
                return m.second(req, args);
            }
        }
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, name);
    }
    self_type& super(std::initializer_list<std::string> names){
        std::for_each(names.begin(), names.end(), [this](const std::string& name){
            super(name);
        });
        return *this;
    }
    self_type& super(const std::string& name, std::string path=""){
        if(path.empty()){
            path = name+".js";
        }
        mixin(name).from(path);
        return *this;
    }
    void mixin(const std::string& name, const std::string& path){
        _mixins.push_back(name);
        _imports.push_back(budho::require(name).from(path));
    }
    mixin_helper mixin(const std::string& name){
        return mixin_helper(*this, name);
    }
    template <typename F>
    detail::method_wrapper<typename udho::internal::reduced_<F>::reduced_function_type> expose(F f){
        return detail::method_wrapper<typename udho::internal::reduced_<F>::reduced_function_type>(udho::internal::reduced(f, static_cast<T*>(this)));
    }
    template <typename F>
    F bind(F f){
        f.set(static_cast<T*>(this));
        return f;
    }
    method_storage_type& methods(){
        return _method_store;
    }
};

template <typename T>
budho::component<T>& operator<<(budho::component<T>& component, const budho::require& imp){
    component._imports.push_back(imp);
    return component;
}

template <typename T, typename M>
budho::detail::methods_helper<T>& operator<<(budho::detail::methods_helper<T>& helper, const M& method){
    helper.add(method);
    return helper;
}

template <typename T>
std::ostream& operator<<(std::ostream& stream, const component<T>& comp){
    return comp.generate(stream);
}

}

#endif // BUDHO_COMPONENT_H
