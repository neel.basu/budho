/*
 * Copyright (c) 2018, Sunanda Bose (Neel Basu) (neel.basu.z@gmail.com) 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 * 
 *  * Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer. 
 *  * Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE. 
 */

#ifndef BUDHO_SERVER_H
#define BUDHO_SERVER_H

#include <iostream>
#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include <udho/util.h>
#include <udho/router.h>
#include <budho/router.h>
#include <budho/component.h>
#include <budho/local.h>
#include <termcolor/termcolor.hpp>
#include <boost/filesystem.hpp>
#include <budho/visitor.h>
#include <fort.hpp>
#include <budho/bundler.h>

namespace budho { 
    class server{
        typedef server self_type;

        int _argc;
        char** _argv;
        boost::asio::io_service _io;
        boost::program_options::options_description _description;
        boost::program_options::variables_map _vm;
        budho::bundler _bundle;     
      public:
        explicit server(int argc, char** argv);
        template <typename RouterT>
        int serve(RouterT& router){
#ifndef BUDHO_BUNDLER
            parse();
            if(_vm.count("help")){
                std::cout << _description << std::endl;
                return 0;
            }else if(_vm.count("table")){
                welcome();
                fort::char_table table;
                table.set_border_style(FT_BASIC_STYLE);
                table << fort::header << "type" << "verb" << "prefix" << "pattern" << "compositor" << "details" << fort::endr;
                router /= budho::visitors::print<budho::visitors::visitable::all>(table);
                std::cout << table.to_string() << std::endl;
                return 0;
            }
            
            int port = _vm["port"].as<int>();
            int threads = _vm["threads"].as<int>();
            std::string host = _vm["host"].as<std::string>();
            std::string document_root = _vm["documents"].as<std::string>();
            try{
                router.listen(_io, port, document_root);
                welcome();
                std::cout << std::endl;
            }catch(const std::exception& ex){
                std::cerr << termcolor::red << ex.what() << termcolor::reset << std::endl;
                return -1;
            }

            run(threads);
#endif
            
#ifdef BUDHO_BUNDLER
            _bundle.parse();
            router /= budho::visitors::target<budho::visitors::visitable::component>([this](const auto& component){
                _bundle.add(component);
            });
            _bundle.pack();
#endif            
            return 0;
        }
        void parse();
        void run(int threads = 1);
        template <budho::resource type, typename... T>
        budho::local<type> local(const T&... args){           
            return budho::local<type>(args...);
        }
        /**
         * Serves a file from document_root if exists
         */
        template <typename... T>
        budho::local<budho::resource::file> file(const T&... args){
            std::string document_root;
            if(_vm.count("documents")){
                document_root = _vm["documents"].as<std::string>();
            }
            return local<budho::resource::file>(args...).base(document_root);
        }
        template <typename... T>
        budho::local<budho::resource::rendered> render(const T&... args){
            std::string template_root;
            if(_vm.count("documents")){
                template_root = _vm["templates"].as<std::string>();
            }
            return local<budho::resource::rendered>(args...).base(template_root);
        }
        
        budho::local<resource::compiled> document(const std::string& path){
            return budho::local<resource::compiled>::document(path);
        }
        template <typename T>
        budho::local<budho::resource::compiled> compiled(const std::string& name){
            return budho::local<budho::resource::compiled>::file<T>(name);
        }
        void welcome() const;
    };
}

#endif //BUDHO_SERVER_H
