/*
 * Copyright (c) 2018, Sunanda Bose (Neel Basu) (neel.basu.z@gmail.com) 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 * 
 *  * Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer. 
 *  * Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE. 
 */


#ifndef BUDHO_BUNDLER_H
#define BUDHO_BUNDLER_H

#include <map>
#include <string>
#include <sstream>
#include <udho/application.h>
#include <boost/filesystem/path.hpp>
#include <boost/program_options.hpp>

namespace budho{

/**
 * @todo write docs
 */
struct bundler{
//     boost::filesystem::path _sources;
    int _argc;
    char** _argv;
    std::map<std::string, std::string> _components;
    boost::program_options::options_description _description;
    boost::program_options::variables_map _vm;
    
    bundler(int argc, char** argv);
    void parse();
    template <typename T>
    void add(const T& component){
        std::stringstream stream;
        component.generate(stream);
        std::string name = component.name();
        std::string content = stream.str();
        _components.insert(std::make_pair(name, content));
    }
    void generate(const boost::filesystem::path& base);
    void pack();
    private:
        void link_all(const boost::filesystem::path& source, const boost::filesystem::path& target);
        void npm_init(const boost::filesystem::path& target);
        void npm_install(const boost::filesystem::path& target);
        void webpack_init(const boost::filesystem::path& target);
        void budho_js(const boost::filesystem::path& target);
        void webpack(const boost::filesystem::path& target);
        void copy(const boost::filesystem::path& source, const boost::filesystem::path& target);
};

}

#endif // BUDHO_BUNDLER_H
