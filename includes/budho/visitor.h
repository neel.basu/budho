/*
 * Copyright (c) 2018, Sunanda Bose (Neel Basu) (neel.basu.z@gmail.com) 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 * 
 *  * Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer. 
 *  * Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE. 
 */

#ifndef BUDHO_VISITOR_H
#define BUDHO_VISITOR_H

#include <udho/router.h>
#include <budho/router.h>
#include <budho/component.h>
#include <budho/local.h>
#include <boost/format.hpp>
#include <fort.hpp>

namespace budho{
    template <typename U>
    struct is_component{
        template <typename V>
        static typename V::component_type test(int);
        template <typename>
        static void test(...);
        enum {value = !std::is_void<decltype(test<U>(0))>::value};
    };
    
    template <typename U>
    struct is_application{
        template <typename V>
        static typename V::application_type test(int);
        template <typename>
        static void test(...);
        enum {value = !std::is_void<decltype(test<U>(0))>::value};
    };
    
    template <typename U>
    struct is_bundle{
        template <typename V>
        static typename V::resource_type test(int);
        template <typename>
        static void test(...);
        enum {value = !std::is_void<decltype(test<U>(0))>::value};
    };
      
    template <typename VisitorT, typename ModuleT, bool IsApplication=is_application<ModuleT>::value, bool IsComponent=is_component<ModuleT>::value, bool IsBundle=is_bundle<ModuleT>::value>
    struct module_evaluator;
    
    template <typename VisitorT, typename ModuleT>
    struct module_evaluator<VisitorT, ModuleT, false, false, false>{
        // bare module
        VisitorT& _visitor;
        
        module_evaluator(VisitorT& visitor): _visitor(visitor){}
        void operator()(ModuleT& modul){
            _visitor._module(modul);
        }
    };
    
    template <typename VisitorT, typename ModuleT>
    struct module_evaluator<VisitorT, ModuleT, false, false, true>{
        // bundle
        VisitorT& _visitor;
        
        module_evaluator(VisitorT& visitor): _visitor(visitor){}
        void operator()(ModuleT& modul){
            _visitor._bundle(modul);
        }
    };
    
    template <typename VisitorT, typename ModuleT>
    struct module_evaluator<VisitorT, ModuleT, true, false, false>{
        // application
        VisitorT& _visitor;
        
        module_evaluator(VisitorT& visitor): _visitor(visitor){}        
        void operator()(ModuleT& app){
            _visitor._application(app);
        }
    };
    
    template <typename VisitorT, typename ModuleT>
    struct module_evaluator<VisitorT, ModuleT, false, true, false>{
        // component
        VisitorT& _visitor;
        
        module_evaluator(VisitorT& visitor): _visitor(visitor){}        
        void operator()(ModuleT& component){
            _visitor._component(component);
        }
    };
    
    template <typename ActorT, typename ModuleT>
    void visit(ActorT& actor, ModuleT& mod){
        module_evaluator<ActorT, ModuleT> ev(actor);
        ev(mod);
    }
    template <typename VisitorT>
    struct visitor: VisitorT{
        using VisitorT::VisitorT;
        
        template <typename ModuleT>
        void operator()(ModuleT& overload){
            visit(*this, overload);
        }
        void operator()(){
            VisitorT::operator()();
        }
    };
        
    namespace visitors{
        struct visitable{
            static const std::uint8_t callable    = (1 << 0);
            static const std::uint8_t bundle      = (1 << 1);
            static const std::uint8_t application = (1 << 2);
            static const std::uint8_t component   = (1 << 3);
            static const std::uint8_t all         = callable | bundle | application | component;
        };
        
        template <std::uint8_t Visitables, typename StreamT>
        struct printing_visitor{
            StreamT& _stream;
            
            printing_visitor(StreamT& stream): _stream(stream){}        
            template <typename ModuleT>
            void _module(const ModuleT& overload){
                if(Visitables & visitable::callable){
                    auto info = overload.info();
                    _stream << boost::format("module %1% %2% -> %3% (%4%)") % info._method % info._pattern % info._fptr % info._compositor << std::endl;
                }
            }
            template <typename AppT>
            void _application(const AppT& app){
                if(Visitables & visitable::application){
                    _stream << boost::format("application %1%") % app._path << std::endl;
                }
            }
            template <typename ComponentT>
            void _component(const ComponentT& component){
                if(Visitables & visitable::component){
                    _stream << boost::format("component %1%") % component.name() << std::endl;
                }
            }
            template <typename BundleT>
            void _bundle(const BundleT& /*bundle*/){
                if(Visitables & visitable::bundle){
                    _stream << boost::format("bundle") << std::endl;
                }
            }
            void operator()(){
                _stream << "pop" << std::endl;
            }
        };
        template <std::uint8_t Visitables>
        struct printing_visitor<Visitables, fort::char_table>{
            fort::char_table& _table;
            std::vector<std::string> _prefixes;
            
            printing_visitor(fort::char_table& table): _table(table){}        
            template <typename ModuleT>
            void _module(const ModuleT& overload){
                if(Visitables & visitable::callable){
                    auto info = overload.info();
                    _table << "module" << info._method << prefix() << info._pattern << info._compositor << info._fptr << fort::endr;
                }
            }
            template <typename AppT>
            void _application(const AppT& app){
                if(Visitables & visitable::application){
                    _table << fort::separator;
                    _table << "app" << "-" << prefix() << app._path << "-" << "-" << fort::endr;
                    _prefixes.push_back(app._path);
                }
            }
            template <typename ComponentT>
            void _component(const ComponentT& component){
                if(Visitables & visitable::component){
                    _table << "component" << "-" << prefix() << "-" << "-" << component.name() << fort::endr;
                }
            }
            template <typename BundleT>
            void _bundle(const BundleT& bundle){
                if(Visitables & visitable::bundle){
                    _table << fort::separator;
                    _prefixes.push_back("^/"+bundle._base+"$");
                    typedef typename BundleT::resource_type res_type;
                    for(const res_type& res: bundle._resources){
                        _table << "dist" << "GET" << prefix() << ("^/"+res._name+"$") << "UNSPECIFIED" << res._size << fort::endr;
                    }
                    operator()();
                }
            }
            void operator()(){
                if(!_prefixes.empty()){
                    _prefixes.pop_back();
                }
                _table << fort::separator;
            }
            std::string prefix() const{
                return boost::algorithm::join(_prefixes, " > ");
            }
        };
        template <typename StreamT>
        using print_all = visitor<printing_visitor<visitable::all,  StreamT>>;
        template <typename StreamT>
        using print_components = visitor<printing_visitor<visitable::component,  StreamT>>;
        template <std::uint8_t Visitables, typename F>
        visitor<printing_visitor<Visitables, F>> print(F& callback){
            return visitor<printing_visitor<Visitables, F>>(callback);
        }
        
        template <typename F, std::uint8_t Visitables, std::uint8_t HasModule = (Visitables & visitable::callable)>
        struct target_module{
            F _callback;
            target_module(F callback): _callback(callback){}
            
            template <typename ModuleT>
            void _module(ModuleT& overload){
                _callback(overload);
            }
        };
        template <typename F, std::uint8_t Visitables>
        struct target_module<F, Visitables, 0>{
            F _callback;
            target_module(F callback): _callback(callback){}
            
            template <typename ModuleT>
            void _module(ModuleT& /*overload*/){}
        };
        
        template <typename F, std::uint8_t Visitables, std::uint8_t HasBundle = (Visitables & visitable::bundle)>
        struct target_bundle{
            F _callback;
            target_bundle(F callback): _callback(callback){}
            
            template <typename BundleT>
            void _bundle(BundleT& overload){
                _callback(overload);
            }
        };
        template <typename F, std::uint8_t Visitables>
        struct target_bundle<F, Visitables, 0>{
            F _callback;
            target_bundle(F callback): _callback(callback){}
            
            template <typename BundleT>
            void _bundle(BundleT& /*overload*/){}
        };
        
        template <typename F, std::uint8_t Visitables, std::uint8_t HasApplication = (Visitables & visitable::application)>
        struct target_application{
            F _callback;
            target_application(F callback): _callback(callback){}
            
            template <typename AppT>
            void _application(AppT& overload){
                _callback(overload);
            }
        };
        template <typename F, std::uint8_t Visitables>
        struct target_application<F, Visitables, 0>{
            F _callback;
            target_application(F callback): _callback(callback){}
            
            template <typename AppT>
            void _application(AppT& /*overload*/){}
        };
        
        template <typename F, std::uint8_t Visitables, std::uint8_t HasApplication = (Visitables & visitable::component)>
        struct target_component{
            F _callback;
            target_component(F callback): _callback(callback){}
            
            template <typename ComponentT>
            void _component(ComponentT& overload){
                _callback(overload);
            }
        };
        template <typename F, std::uint8_t Visitables>
        struct target_component<F, Visitables, 0>{
            F _callback;
            target_component(F callback): _callback(callback){}
            
            template <typename ComponentT>
            void _component(ComponentT& /*overload*/){}
        };
        
        template <std::uint8_t Visitables, typename F>
        struct targetted_visitor: target_module<F, Visitables>, target_bundle<F, Visitables>, target_application<F, Visitables>, target_component<F, Visitables>{
            targetted_visitor(F callback): target_module<F, Visitables>(callback), target_bundle<F, Visitables>(callback), target_application<F, Visitables>(callback), target_component<F, Visitables>(callback){}        
            void operator()(){}
        };
               
        template <std::uint8_t Visitables, typename F>
        using targeted = visitor<targetted_visitor<Visitables, F>>;
        template <std::uint8_t Visitables, typename F>
        visitor<targetted_visitor<Visitables, F>> target(F callback){
            return visitor<targetted_visitor<Visitables, F>>(callback);
        }
    }
}

#endif // BUDHO_VISITOR_H
