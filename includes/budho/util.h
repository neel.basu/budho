/*
 * Copyright (c) 2018, Sunanda Bose (Neel Basu) (neel.basu.z@gmail.com) 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 * 
 *  * Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer. 
 *  * Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE. 
 */

#ifndef BUDHO_UTIL_H
#define BUDHO_UTIL_H

#include <nlohmann/json.hpp>
#include <boost/lexical_cast.hpp>

namespace budho{
    
    template <typename T>
    struct conv{
        typedef T coerced_type;
        static coerced_type coerce(T v){return v;}
    };
    
namespace util{
    
    std::string stringify(const nlohmann::json& value);
    
    template <typename T>
    struct coerce_helper{
        static T apply(const nlohmann::json& value){
            return boost::lexical_cast<T>(stringify(value));
//             nlohmann::json::value_t type = value.type();
//             switch(type){
//                 case nlohmann::json::value_t::null:
//                     if(std::is_convertible<nullptr_t, T>::value){
//                         return nullptr;
//                     }else{
//                         return boost::lexical_cast<T>(stringify(value, defv));
//                     }
//                     break;
//                 case nlohmann::json::value_t::boolean:
//                     if(std::is_convertible<bool, T>::value){
//                         return value.get<bool>();
//                     }else{
//                         return boost::lexical_cast<T>(stringify(value, defv));
//                     }
//                     break;
//                 case nlohmann::json::value_t::number_integer:
//                     if(std::is_convertible<int, T>::value){
//                         return value.get<int>();
//                     }else{
//                         return boost::lexical_cast<T>(stringify(value, defv));
//                     }
//                     break;
//                 case nlohmann::json::value_t::number_unsigned:
//                     if(std::is_convertible<unsigned, T>::value){
//                         return value.get<unsigned>();
//                     }else{
//                         return boost::lexical_cast<T>(stringify(value, defv));
//                     }
//                     break;
//                 case nlohmann::json::value_t::number_float:
//                     if(std::is_convertible<double, T>::value){
//                         return value.get<double>();
//                     }else{
//                         return boost::lexical_cast<T>(stringify(value, defv));
//                     }
//                     break;
//                 case nlohmann::json::value_t::string:
//                     if(std::is_convertible<std::string, T>::value){
//                         return value.get<std::string>();
//                     }else{
//                         return boost::lexical_cast<T>(stringify(value, defv));
//                     }
//                     break;
//                 case nlohmann::json::value_t::object:
//                     if(std::is_convertible<nlohmann::json, T>::value){
//                         return value;
//                     }else{
//                         return boost::lexical_cast<T>(stringify(value, defv));
//                     }
//                     break;
//                 case nlohmann::json::value_t::array:
//                     if(std::is_convertible<nlohmann::json, T>::value){
//                         return value;
//                     }else{
//                         return boost::lexical_cast<T>(stringify(value, defv));
//                     }
//                     break;
//                 default:
//                     return defv;
//             }
        }
    };
    
    template <typename T>
    T coerce(const nlohmann::json& value){
        return coerce_helper<T>::apply(value);
    }
    
}
    
}

#endif // BUDHO_UTIL_H
