/*
 * Copyright (c) 2018, Sunanda Bose (Neel Basu) (neel.basu.z@gmail.com) 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 * 
 *  * Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer. 
 *  * Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE. 
 */

#ifndef BUDHO_ROUTER_H
#define BUDHO_ROUTER_H

#include <udho/router.h>
#include <udho/util.h>
#include <budho/util.h>
#include <budho/local.h>

namespace budho{
namespace embedded{
    
// udho::overload_group<overload_group<module_overload<udho::response_type (*)(const udho::request_type&)>, void>, udho::app_<budho::embedded::packed>>
    
}
}

namespace budho{    
    template <budho::resource ResourceT>
    struct resource_bundle{    
        typedef resource_bundle<ResourceT> self_type;
        typedef budho::local<ResourceT> resource_type;
        typedef typename budho::embedded::representation<resource::file, ResourceT>::response_type response_type;
        
        std::string _base;
        std::vector<resource_type> _resources;
        
        resource_bundle(){
            // insert(budho::buffered("dist/main.js", (const unsigned char*)"hallo world", 11));
        }
        resource_bundle(const self_type& other): _base(other._base), _resources(other._resources){}

        self_type& insert(const resource_type& resource){
            _resources.push_back(resource);
            return *this;
        }
        self_type& operator=(const std::string& base){
            _base = base;
            return *this;
        }
        /**
        * check number of arguments supplied on runtime and number of arguments with which this overload has been prepared at compile time.
        */
        bool feasible(boost::beast::http::verb request_method, const std::string& subject) const{
            std::string subject_decoded = udho::util::urldecode(subject);
            if(request_method == boost::beast::http::verb::get){
                for(const resource_type& res: _resources){
                    std::string pattern;
                    if(!_base.empty()){
                        pattern = "^/"+_base+"/"+res._name+"$";
                    }else{
                        pattern = "^/"+res._name+"$";
                    }
                    return boost::u32regex_search(subject_decoded, boost::make_u32regex(pattern));
                }
            }
            return false;
        }
        template <typename T>
        response_type operator()(const T& value, const std::string& subject){
            std::vector<std::string> args;
            boost::smatch caps;
            try{
                std::string subject_decoded = udho::util::urldecode(subject);
                for(const resource_type& res: _resources){
                    std::string pattern;
                    if(!_base.empty()){
                        pattern = "^/"+_base+"/"+res._name+"$";
                    }else{
                        pattern = "^/"+res._name+"$";
                    }
                    if(boost::u32regex_search(subject_decoded, caps, boost::make_u32regex(pattern))){
                        std::copy(caps.begin()+1, caps.end(), std::back_inserter(args));
                        return res.template as<budho::resource::file>().response(value);
                    }
                }
            }catch(std::exception& ex){
                std::cout << "ex: " << ex.what() << std::endl;
            }
            return udho::failure_callback(value);
        }
        udho::module_info info() const{
            udho::module_info inf;
            inf._method = boost::beast::http::verb::get;
            return inf;
        }
        private:
            self_type& operator=(const self_type& other){
                _base = other._base;
                return *this;
            }
    };
    
    /**
    * router maps HTTP requests with the callbacks
    * @code
    * auto router = budho::router()
    *      | (budho::get(add).plain()   = "^/add/(\\d+)/(\\d+)$")
    *      | (budho::get(hello).plain() = "^/hello$");
    * @endcode
    * @ingroup routing
    */
    template <typename LoggerT=udho::null_logger>
    struct router: public udho::overload_group<resource_bundle<budho::resource::buffered>, udho::overload_terminal<LoggerT>>{
        /**
        * cnstructs a router
        */
        template <typename... Args>
        router(Args... args): udho::overload_group<resource_bundle<budho::resource::buffered>, udho::overload_terminal<LoggerT>>(resource_bundle<budho::resource::buffered>(), args...){}
        resource_bundle<budho::resource::buffered>& bundle(){
            return udho::overload_group<resource_bundle<budho::resource::buffered>, udho::overload_terminal<LoggerT>>::_overload;
        }
    };
       
    template <typename U, typename V, typename F>
    udho::overload_group<U, V>& operator/=(udho::overload_group<U, V>& group, F fixture){
        group.eval(fixture);
        return group;
    }
    
    template <typename T>
    auto get(T arg){
        typedef typename conv<T>::coerced_type coreced_type;
        return udho::get(std::forward<coreced_type>(conv<T>::coerce(arg)));
    }
    template <typename T>
    auto post(T arg){
        typedef typename conv<T>::coerced_type coreced_type;
        return udho::post(std::forward<coreced_type>(conv<T>::coerce(arg)));
    }
    template <typename T>
    auto head(T arg){
        typedef typename conv<T>::coerced_type coreced_type;
        return udho::head(std::forward<coreced_type>(conv<T>::coerce(arg)));
    }
    template <typename T>
    auto put(T arg){
        typedef typename conv<T>::coerced_type coreced_type;
        return udho::put(std::forward<coreced_type>(conv<T>::coerce(arg)));
    }
    template <typename T>
    auto del(T arg){
        typedef typename conv<T>::coerced_type coreced_type;
        return udho::del(std::forward<coreced_type>(conv<T>::coerce(arg)));
    }
}


#endif /* BUDHO_ROUTER_H */
