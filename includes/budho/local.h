/*
 * Copyright (c) 2018, Sunanda Bose (Neel Basu) (neel.basu.z@gmail.com) 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 * 
 *  * Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer. 
 *  * Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE. 
 */

#ifndef BUDHO_LOCAL_H
#define BUDHO_LOCAL_H

#include <iterator>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <udho/router.h>
#include <udho/session.h>
#include <udho/page.h>
#include <nlohmann/json.hpp>
#include <inja/inja.hpp>
#include <cmrc/cmrc.hpp>
#include <budho/util.h>

CMRC_DECLARE(documents);
CMRC_DECLARE(templates);

#define BUDHO_RESOURCE_DECLARE(x) CMRC_DECLARE(x);                  \
    namespace budho{namespace embedded{        \
        struct x{                                                   \
            static auto fs(){return cmrc::x::get_filesystem();}     \
        };                                                          \
    } }

namespace budho{

namespace http = boost::beast::http;
    
enum class resource{
    file,
    rendered,
    compiled,
    buffered
};

template <resource type>
class local;

/**
 * references a local file from disk by file path
 */
template <>
struct local<resource::file>{
    typedef local<resource::file> self_type;
    typedef boost::function<http::response<http::file_body> (const udho::request_type&)> function_type;
    
    boost::filesystem::path _base;
    boost::filesystem::path _path;
    std::string _mime;
    
    explicit local(const std::string& path): _path(path){}
    self_type& base(const boost::filesystem::path& base){
        _base = base;
        return *this;
    }
    self_type& as(const std::string& mime){
        _mime = mime;
        return *this;
    }
    http::response<http::file_body> response(const udho::request_type& req) const{
        boost::filesystem::path path = _base / _path;
        boost::beast::string_view mime = udho::internal::mime_type(path.string());
        boost::beast::error_code err;
        http::file_body::value_type body;
        body.open(path.c_str(), boost::beast::file_mode::scan, err);
        if(err == boost::system::errc::no_such_file_or_directory){
            throw udho::exceptions::http_error(boost::beast::http::status::not_found, path.string());
        }
        if(err){
            throw udho::exceptions::http_error(boost::beast::http::status::internal_server_error, path.string());
        }
        auto const size = body.size();
        http::response<http::file_body> res{std::piecewise_construct, std::make_tuple(std::move(body)), std::make_tuple(http::status::ok, req.version())};
        res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(http::field::content_type, _mime.empty() ? mime : _mime);
        res.content_length(size);
        res.keep_alive(req.keep_alive());
        return res;
    }
    auto operator()(const udho::request_type& req) const{
        return response(req);
    }
};

typedef local<resource::file> file;

/**
 * references a local file from disk by file path that can be used as inja template
 */
template <>
struct local<resource::rendered>{
    typedef local<resource::rendered> self_type;
    typedef boost::function<http::response<http::string_body> (const udho::request_type&)> function_type;
    
    boost::filesystem::path _base;
    boost::filesystem::path _path;
    nlohmann::json _data;
    mutable inja::Environment _env;
    std::string _mime;
    
    explicit local(const std::string& path): _path(path){}
    local(const self_type& other): _base(other._base), _path(other._path), _data(other._data), /*_env(std::move(other._env)),*/ _mime(other._mime){}
    self_type& base(const boost::filesystem::path& base){
        _base = base;
        return *this;
    }
    self_type& with(const nlohmann::json& data){
        _data = data;
        return *this;
    }
    self_type& when(inja::Environment&& env){
        _env = std::move(env);
        return *this;
    }
    self_type& as(const std::string& mime){
        _mime = mime;
        return *this;
    }
    http::response<http::string_body> response(const udho::request_type& req) const{
        boost::filesystem::path path = _base / _path;
        boost::beast::string_view mime = udho::internal::mime_type(path.string());
        if(!boost::filesystem::exists(path)){
            throw udho::exceptions::http_error(boost::beast::http::status::not_found, path.string());
        }
        std::ifstream stream(path.c_str());
        std::string content(std::istreambuf_iterator<char>{stream}, {});
        std::string rendered = _env.render(content, _data);
        http::response<http::string_body> res{http::status::ok, req.version()};
        res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(http::field::content_type, _mime.empty() ? mime : _mime);
        res.keep_alive(req.keep_alive());
        res.body() = rendered;
        res.prepare_payload();
        return res;
    }
    auto operator()(const udho::request_type& req) const{
        return response(req);
    }
};

typedef local<resource::rendered> rendered;

namespace embedded{
    template <resource type, resource source_type>
    struct representation;
}

/**
 * references a cmrc embedded file by path
 */
template <>
struct local<resource::compiled>{   
    cmrc::embedded_filesystem _filesystem;
    std::string _name;
    
    local(const cmrc::embedded_filesystem& fs, const std::string& name): _filesystem(fs), _name(name){}
    local(const local<resource::compiled>& other): _filesystem(other._filesystem), _name(other._name){}
    template <resource type>
    embedded::representation<type, resource::compiled> as() const{
        return budho::embedded::representation<type, resource::compiled>(*this);
    }
    bool exists() const{
        return _filesystem.exists(_name);
    }
    cmrc::file open() const{
        return _filesystem.open(_name);
    }
    std::string name() const{
        return _name;
    }
    /**
     * local<resource::compiled>::file<budho::embedded::foo>("filename.txt")
     */
    template <typename T>
    static local<resource::compiled> file(const std::string& name){
        return local<resource::compiled>(T::fs(), name);
    }
    static local<resource::compiled> document(const std::string& name){
        return local<resource::compiled>(cmrc::documents::get_filesystem(), name);
    }
};

typedef local<resource::compiled> compiled;

/**
 * references a embedded file by const buffer
 */
template <>
struct local<resource::buffered>{   
    std::string _name;
    const unsigned char* _buff;
    std::size_t _size;
    
    local(const std::string& name, const unsigned char* buff, std::size_t size): _name(name), _buff(buff), _size(size){}
    template <resource type>
    embedded::representation<type, resource::buffered> as() const{
        return budho::embedded::representation<type, resource::buffered>(*this);
    }    
    static local<resource::buffered> file(const std::string& name, const unsigned char* buff, std::size_t size){
        return local<resource::buffered>(name, buff, size);
    }
    const unsigned char* begin() const{
        return _buff;
    }
    const unsigned char* end() const{
        return _buff + _size;
    }
    std::string name() const{
        return _name;
    }
};

typedef local<resource::buffered> buffered;

namespace embedded{
  
/**
 * represents a cmrc embeedded file with mime type
 */
template <>
struct representation<resource::file, resource::compiled>{
    typedef boost::function<http::response<http::string_body> (const udho::request_type&)> function_type;
    typedef representation<resource::file, resource::compiled> self_type;
    typedef http::response<http::string_body> response_type;
    
    local<resource::compiled> _local;
    std::string _mime;
    
    representation(const local<resource::compiled>& local): _local(local){}
    representation(const self_type& other): _local(other._local), _mime(other._mime){}
    self_type& as(const std::string& mime){
        _mime = mime;
        return *this;
    }
    std::string content() const{
        return std::string(_local.open().cbegin(), _local.open().cend());
    }
    http::response<http::string_body> response(const udho::request_type& req) const{
        if(!_local.exists()){
            throw udho::exceptions::http_error(boost::beast::http::status::not_found, _local._name);
        }
        boost::beast::string_view mime = udho::internal::mime_type(_local._name);
        cmrc::file file = _local.open();
        std::string content(file.cbegin(), file.cend());
        http::response<http::string_body> res{http::status::ok, req.version()};
        res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(http::field::content_type, _mime.empty() ? mime : _mime);
        res.keep_alive(req.keep_alive());
        res.body() = content;
        res.prepare_payload();
        return res;
    }
    auto operator()(const udho::request_type& req) const{
        return response(req);
    }
};

template <>
struct representation<resource::file, resource::buffered>{
    typedef boost::function<http::response<http::string_body> (const udho::request_type&)> function_type;
    typedef representation<resource::file, resource::buffered> self_type;
    typedef http::response<http::string_body> response_type;
    
    local<resource::buffered> _local;
    std::string _mime;
    
    representation(const local<resource::buffered>& local): _local(local){}
    self_type& as(const std::string& mime){
        _mime = mime;
        return *this;
    }
    std::string content() const{
        return std::string(_local.begin(), _local.end());
    }
    http::response<http::string_body> response(const udho::request_type& req) const{
        boost::beast::string_view mime = udho::internal::mime_type(_local._name);
        std::string content(_local.begin(), _local.end());
        http::response<http::string_body> res{http::status::ok, req.version()};
        res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(http::field::content_type, _mime.empty() ? mime : _mime);
        res.keep_alive(req.keep_alive());
        res.body() = content;
        res.prepare_payload();
        return res;
    }
    auto operator()(const udho::request_type& req) const{
        return response(req);
    }
};

/**
 * represents a cmrc embeedded file as template
 */
template <>
struct representation<resource::rendered, resource::compiled>{
    typedef representation<resource::rendered, resource::compiled> self_type;
    typedef boost::function<http::response<http::string_body> (const udho::request_type&)> function_type;
    
    const local<resource::compiled>& _local;
    nlohmann::json _data;
    mutable inja::Environment _env;
    std::string _mime;
    
    representation(const local<resource::compiled>& local): _local(local){}
    self_type& with(const nlohmann::json& data){
        _data = data;
        return *this;
    }
    self_type& when(inja::Environment&& env){
        _env = std::move(env);
        return *this;
    }
    self_type& as(const std::string& mime){
        _mime = mime;
        return *this;
    }
    http::response<http::string_body> response(const udho::request_type& req) const{
        if(!_local.exists()){
            throw udho::exceptions::http_error(boost::beast::http::status::not_found, _local._name);
        }
        boost::beast::string_view mime = udho::internal::mime_type(_local._name);
        cmrc::file file = _local.open();
        std::string content(file.cbegin(), file.cend());
        std::string rendered = _env.render(content, _data);
        http::response<http::string_body> res{http::status::ok, req.version()};
        res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(http::field::content_type, _mime.empty() ? mime : _mime);
        res.keep_alive(req.keep_alive());
        res.body() = rendered;
        res.prepare_payload();
        return res;
    }
    auto operator()(const udho::request_type& req) const{
        return response(req);
    }
};

}

template <resource type>
budho::embedded::representation<resource::rendered, type> operator<(const local<type>& res, const nlohmann::json& data){
    return res.template as<budho::resource::rendered>().with(data);
}

template <>
struct conv<local<resource::compiled>>{
    typedef embedded::representation<resource::file, resource::compiled> coerced_type;
    
    static coerced_type coerce(const local<resource::compiled>& x){
        return coerced_type(x);
    }
};

template <>
struct conv<local<resource::buffered>>{
    typedef embedded::representation<resource::file, resource::buffered> coerced_type;
    
    static coerced_type coerce(const local<resource::buffered>& x){
        return coerced_type(x);
    }
};

}

#endif // BUDHO_LOCAL_H
