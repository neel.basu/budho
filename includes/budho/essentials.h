/*
 * Copyright (c) 2018, Sunanda Bose (Neel Basu) (neel.basu.z@gmail.com) 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 * 
 *  * Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer. 
 *  * Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE. 
 */

#ifndef BUDHO_ESSENTIALS_H
#define BUDHO_ESSENTIALS_H

#include <string>
#include <vector>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <cmrc/cmrc.hpp>

namespace budho{

/**
 * import a javascript module
 */
struct require{
    enum Mode{
        unspecified,
        all,
        self
    };
    
    std::vector<std::string> _things;
    bool _braced;
    Mode _mode;
    std::string _path;
    
    /**
     * require({"x", "y"}).from("./path.js")
     */
    require(const std::initializer_list<std::string>& things);
    /**
     * require("x").from("./path.js")
     */
    require(const std::string& thing);
    /**
     * require().from("./path.js")
     * require(require::all).from("./path.js")
     */
    require(Mode mode=unspecified);
    require& from(const std::string& path);
    
    template <typename StreamT>
    StreamT& write(StreamT& stream){
        if(_mode == unspecified && _braced){
            stream << boost::format("import {%1%} from \"%2%\";") % boost::algorithm::join(_things, ",") % _path << std::endl;
        }else if(_mode == unspecified){
            stream << boost::format("import %1% from \"%2%\";") % _things[0] % _path << std::endl;
        }else if(_mode == all){
            stream << boost::format("import * from \"%1%\";") % _path << std::endl;
        }else if(_mode == self){
            stream << boost::format("import \"%1%\";") % _path << std::endl;
        }
        return stream;
    }
};

struct mixin{
    
};

void reflect(const cmrc::embedded_filesystem& fs);

}

#endif // BUDHO_ESSENTIALS_H
