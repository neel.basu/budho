/*
 * Copyright (c) 2018, Sunanda Bose (Neel Basu) (neel.basu.z@gmail.com) 
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 * 
 *  * Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer. 
 *  * Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE. 
 */

#ifndef BUDHO_PACKER_H
#define BUDHO_PACKER_H

#include <vector>
#include <budho/visitor.h>
#include <udho/router.h>
#include <budho/router.h>
#include <budho/local.h>

namespace budho{
namespace fixtures{
    template <budho::resource ResourceT>
    struct dist{
        typedef dist<ResourceT> self_type;
        typedef budho::local<ResourceT> resource_type;
        
        std::string _base;
        std::vector<resource_type> _resources;
        
        dist(std::string base=""): _base(base){}
        self_type& insert(const resource_type& resource){
            _resources.push_back(resource);
            return *this;
        }
        
        template <typename BundleT>
        void operator()(BundleT& bundle){
            bundle._base = _base;
            for(const resource_type& res: _resources){
                bundle.insert(res);
            }
        }
    };
    template <budho::resource ResourceT>
    using packer = budho::visitors::targeted<budho::visitors::visitable::bundle, dist<ResourceT>>;
}

budho::fixtures::packer<budho::resource::buffered> pack(std::string base="");

#ifdef BUDHO_BUNDLER
budho::fixtures::packer<budho::resource::buffered> pack(std::string base){
    budho::fixtures::dist<budho::resource::buffered> distribution(base);
    return budho::visitors::target<budho::visitors::visitable::bundle>(distribution);
}
#endif

}

#endif // BUDHO_PACKER_H
