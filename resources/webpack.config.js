const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const { BudhoPlugin } = require("./budho");

module.exports = {
    // TODO add entry points 
    entry: {
        main: './index.js',
    },
    output: {
        filename: '[name].js',
        path: path.join(process.cwd(), 'dist')
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader'
                ]
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
        new BudhoPlugin()
    ],
    resolve: {
        symlinks: false,
        alias: {
            vue: 'vue/dist/vue.esm.js'
        }
    }
}
