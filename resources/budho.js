const path = require('path');
const fs = require('fs');

class BudhoPlugin {
    dump(buffer) {
        let lines = []
        for (let i = 0; i < buffer.length; i += 16) {
            let block = buffer.slice(i, i + 16) // cut buffer into blocks of 16

            let hexArray = []
            for (let value of block) {
                const code = value.toString(16).padStart(2, '0');
                 hexArray.push('0x'+code)
            }
            
            let hexString =
                hexArray.length > 8
                ? hexArray.slice(0, 8).join(', ') + ',  ' + hexArray.slice(8).join(', ')
                : hexArray.join(', ')

            let line = hexString
            lines.push(line)
        }
        return "{\n\t"+lines.join(',\n\t')+"\n}";
    }
    name(filename) {
        const regex = /[\/\.]/g;
        return filename.replace(regex, '_');
    }
    apply(compiler) {
        compiler.hooks.emit.tapAsync('BudhoPlugin', (compilation, callback) => {
            const base = path.relative(process.cwd(), compilation.compiler.outputPath);
            let blobs = [];
            for(var asset in compilation.assets){
                const path = base + "/" + asset;
                const buffer = Buffer.from(compilation.assets[asset].source(), 'ascii');
                blobs.push({
                    path: path,
                    name: this.name(path),
                    buff: this.dump(buffer),
                    size: buffer.byteLength,
                    header: asset+'.h'
                });
            }
            const content = JSON.stringify({
                base: base,
                blobs: blobs
            });
            compilation.assets['packing.json'] = {
                source: function() {
                    return content;
                },
                size: function() {
                    return content.length;
                }
            };
            for(let blob of blobs){
                const template = '\
                #ifndef HEADER_{{hname}}                               \n\
                #define HEADER_{{hname}}                               \n\
                \n\
                extern const unsigned char {{name}}[] = {{buff}};      \n\
                extern const unsigned int {{name}}_size = {{size}};    \n\
                extern const char {{name}}_path[] = "{{path}}";        \n\
                \n\
                #endif // HEADER_{{hname}}                             \n\
                '.replace(/^\s{16}/gm, '');
                const content = template.replace(/{{name}}/g, blob.name)
                                        .replace(/{{hname}}/g, blob.name.toUpperCase())
                                        .replace(/{{buff}}/g, blob.buff)
                                        .replace(/{{size}}/g, blob.size)
                                        .replace(/{{path}}/g, blob.path);
                compilation.assets[blob.header] = {
                    source: function() {
                        return content;
                    },
                    size: function() {
                        return content.length;
                    }
                }
            }
            const headers   = ['#include <budho/packer.h>', '#include <string>', ''].join('\n')
            const buffers   = blobs.map(b => '#include "{{base}}/{{header}}"'
                                                .replace(/{{header}}/g, b.header)
                                                .replace(/{{base}}/g, base)
                                       ).join('\n');
            const resources = blobs.map(b => 'distribution.insert(budho::buffered("{{path}}", (const unsigned char*){{name}}, {{size}}));'
                                                .replace(/{{name}}/g, b.name)
                                                .replace(/{{size}}/g, b.size)
                                                .replace(/{{path}}/g, b.path)
                                       ).join('\n                    ');
            const app = '\
                namespace budho{                                                                     \n\
                \n\
                budho::fixtures::packer<budho::resource::buffered> pack(std::string base){           \n\
                    budho::fixtures::dist<budho::resource::buffered> distribution(base);             \n\
                    {{resources}}                                                                    \n\
                    return budho::visitors::target<budho::visitors::visitable::bundle>(distribution);\n\
                }                                                                                    \n\
                \n\
                }                                                                                    \n\
                '.replace(/{{resources}}/g, resources)
                .replace(/^\s{16}/gm, '');
                
            const cpp = [headers, "namespace{\n"+buffers+"\n}", app].join('\n');
            fs.writeFileSync('packed.cpp', cpp);
            callback();
        });
    }
}

module.exports = {
    BudhoPlugin
}
