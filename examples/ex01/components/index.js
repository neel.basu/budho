import Vue from 'vue'
import HelloComponent from "./HelloComponent.vue"

new Vue({
    el: '#main',
    components: {
        'HelloPlanet': HelloComponent
    }
})
