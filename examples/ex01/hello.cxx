/*
 * Copyright (c) 2020, <copyright holder> <email>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY <copyright holder> <email> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <copyright holder> <email> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "hello.hxx"
#include <rumal/declares.hpp>
#include <boost/lexical_cast.hpp>

HelloComponent::HelloComponent(): base_type("HelloComponent"){
    super("_HelloComponent");
    watch("index");
    
    methods() << bind(add).plain()
              << bind(mult).plain();
}

nlohmann::json HelloComponent::props() const{
    return {"index"};
}

rumal::fragment<> HelloComponent::html() const{
    using namespace rumal::html::attrs;
    using namespace rumal::html::tags;
    
    return div(_class("hello-component"),
                div(_class("control"), 
                    span(_class("control-current"), "{{index}}"),
                    button(_class("control-inc") / vue::on("click", "index += 1"), "+"),
                    button(_class("control-dec") / vue::on("click", "index -= 1"), "-")
                ),
                div(vue::_if("loaded") / _class("body"),
                    div(_class("message"),
                        span("{{content.greeting}}"),
                        button(vue::on("click", "add([2, 3], function(r){ping(r);})"), "{{content.planet}}")
                    )
                )
            );
}

rumal::fragment<rumal::css::scoped> HelloComponent::css() const{
    using namespace rumal::css;
    using namespace rumal::css::props;
    
    return select(".hello-component",
                  position("relative")
                / display("block")
                / width("90px")
                / border("1px solid #dcdcdc"),
                select(".control", 
                      position("relative")
                    / display("block")
                    / width("100%")
                    / backgroundColor("#dcdcdc")
                ),
                select(".body", 
                      position("relative") 
                    / display("block")
                    / width("100%"),
                    select(".message", 
                          position("relative")
                        / display("block")
                        / width("100%")
                        / rumal::css::prop("font-variant-caps", "petite-caps")
                    )
                )
            )
            / select(".control-inc",
                      position("relative")
                    / border("0px none")
                    / background("transparent")
                    / cssFloat("right")
            )
            / select(".control-dec",
                      position("relative")
                    / border("0px none")
                    / background("transparent")
                    / cssFloat("right")
            )
            / select("control-current",
                      position("relative")
            );
}

// https://api.le-systeme-solaire.net/rest/bodies/mars
nlohmann::json HelloComponent::data(const udho::request_type& /*req*/, nlohmann::json props, nlohmann::json /*d*/){
    typedef std::vector<std::string> container_type;
    typedef container_type::size_type size_type;
    
    container_type planets{"Murcury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune", "Pluto", "Planet X"};
    size_type index = budho::util::coerce<size_type>(props["index"]);
    std::string planet_name = "Unknown";
    if(index > 0 && index < planets.size()){
        planet_name = planets[index-1];
    }
    
    return {
        {"greeting", "Hi"},
        {"planet", planet_name}
    };
}

int HelloComponent::addition(const udho::request_type& /*req*/, int a, int b){
    return a + b;
}

int HelloComponent::multiplication(const udho::request_type& /*req*/, int a, int b){
    return a * b;
}
