#include <iostream>
#include <rumal/rumal.hpp>
#include <budho/component.h>
#include <nlohmann/json.hpp>
#include <budho/server.h>
#include <boost/program_options.hpp>
#include <budho/local.h>
#include <budho/router.h>
#include <budho/packer.h>
#include <udho/logging.h>
#include "hello.hxx"

std::string hello(udho::request_type /*req*/){
    return "Hello World";
}

int main(int argc, char** argv){
    budho::server server(argc, argv);
    
    nlohmann::json data({
        {"planet", "earth"}
    });
        
    auto router = budho::router<udho::loggers::plain>()
        | (budho::get(server.render("test.tmpl.txt").with(data)) = "^/test")
        | (budho::get(server.file("test.tmpl.txt")) = "^/tmpl")
        | (budho::get(server.document("/documents/index.html")) = "^/$")
        | (budho::get(&hello).plain() = "^/hello")
        | (udho::app<HelloComponent>());

    server.serve(router /= budho::pack());
    return 0;
}
