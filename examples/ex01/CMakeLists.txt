cmake_minimum_required(VERSION 3.0)

project(budho-ex01)

SET(CMAKE_CXX_STANDARD 17)

set(BUDHO_EXAMPLES_EX01_SOURCES
    hello.cxx
    main.cpp
)

set(BUDHO_EXAMPLES_EX01_DOCUMENTS
    documents/index.html
)

add_executable(budho-ex01 ${BUDHO_EXAMPLES_EX01_SOURCES})
BUDHO_RESOURCES(TARGET budho-ex01-res DOCUMENTS ${BUDHO_EXAMPLES_EX01_DOCUMENTS})
target_link_libraries(budho-ex01 budho budho-ex01-res)
BUDHO_ATTACH(TARGET budho-ex01 COMPONENTS components)
