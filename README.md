# Budho a component ready Web development Framework
---
<pre>

           ◉ ◉
    ◢◣    ◉   ◢◣          
   ◢◤◢◣    ◉ ◢◤◢◣           
 ◢◤◢◣◢◤◢◣  ◢◤◢◣◢◤◢◣
        ◢◣
       ◢◤◢◣▮▮▮▮▮▮▮▮▮▮▮▮▮

</pre>
### Build

```
git clone git@gitlab.com:neel.basu/budho.git
cd budho
mkdir build
cd build
cmake ..
make
```

The above will build the budho library as well as an example web application in examples/ex01. To run that application

```
cd build/examples/ex01
./budho-ex01
```

